//
//  ManaButton.swift
//  Magic support
//
//  Created by Toni García Alhambra on 10/01/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

struct ManaButton {
    private let manaColors: [String: UIColor] =
        ["yellow": UIColor(red:0.90, green:0.62, blue:0.09, alpha:1.0),
         "blue": UIColor(red:0.16, green:0.27, blue:0.57, alpha:1.0),
         "black": UIColor(red:0.27, green:0.02, blue:0.39, alpha:1.0),
         "green": UIColor(red:0.02, green:0.40, blue:0.21, alpha:1.0),
         "red": UIColor(red:0.49, green:0.00, blue:0.11, alpha:1.0)]
    
    private var buttonSelected: UIButton?
    private var buttonPressed: UIButton?
    
    func getColor(from manaButton: UIButton) -> UIColor {
        return manaColors[manaButton.titleLabel!.text!]!
    }
    
    mutating func newSelection(_ button: UIButton) {
        guard var enabledPath = button.titleLabel!.text else {fatalError("No se ha conseguido obtener la foto del botón")}
        enabledPath.append("_fill") // textLabel establecido en storyBoard. ej: "green_fill"
        button.setImage(UIImage(named: enabledPath), for: .normal)
        
        if buttonSelected != nil && buttonSelected != button {
            var disablePath = buttonSelected!.titleLabel!.text
            disablePath!.append("_empty") // textLabel establecido en storyBoard. ej: "green_empty"
            buttonSelected!.setImage(UIImage(named: disablePath!), for: .normal)
        }
        
        buttonSelected = button
    }
}
