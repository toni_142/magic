//
//  GameConfigurationViewController.swift
//  Magic support
//
//  Created by Toni García Alhambra on 08/01/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class GameConfigurationViewController: UIViewController {
    
    @IBOutlet weak var startButton: RoundedButton!
    
    private var lifeButtonSelected: ButtonSelectable?
    private var playersButtonSelected: ButtonSelectable?
    public var selectedColor: UIColor?
    public var numberOfLifes: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Buttons
    @IBAction func lifeButtonPressed(_ sender: ButtonSelectable) {
        changeSelectedValueTo(sender, type: lifeButtonSelected)
        lifeButtonSelected = sender
        
        if playersButtonSelected != nil {
            startButton.backgroundColor = selectedColor
            startButton.isEnabled = true
        }
        
        numberOfLifes = sender.titleLabel?.text
    }
    
    @IBAction func playersButtonPressed(_ sender: ButtonSelectable) {
        changeSelectedValueTo(sender, type: playersButtonSelected)
        playersButtonSelected = sender
        
        if lifeButtonSelected != nil {
            startButton.backgroundColor = selectedColor
            startButton.isEnabled = true
        }
    }
    
    @IBAction func startButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "goToTwoGameView", sender: self)
    }
    
    // MARK: - Functions
    func changeSelectedValueTo(_ newSelection: ButtonSelectable, type: ButtonSelectable?) {
        newSelection.hasStroke = false
        newSelection.backgroundColor = selectedColor

        if newSelection != type {
            type?.hasStroke = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! TwoPlayersGameViewController
        destinationVC.playerTwoColor = selectedColor
        destinationVC.life = numberOfLifes
    }
}
