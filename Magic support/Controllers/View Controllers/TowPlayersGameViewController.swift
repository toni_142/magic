//
//  TowPlayersGameViewController.swift
//  Magic support
//
//  Created by Toni García Alhambra on 08/01/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class TwoPlayersGameViewController: UIViewController {
    
    @IBOutlet weak var playerOneView: UIView!
    @IBOutlet weak var playerTwoView: UIView!
    
    @IBOutlet weak var playerOneLife: UILabel!
    @IBOutlet weak var playerTwoLife: UILabel!
    
    @IBOutlet weak var pickColorView: UIView!
    
    private var lifeManager = LifeManager()
    private var manaButton = ManaButton()
    
    public var playerOneColor: UIColor?
    public var playerTwoColor: UIColor?
    public var life: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateView()
    }
    
    // MARK: - Buttons
    @IBAction func manaButtonPressed(_ sender: UIButton) {
        playerOneColor = manaButton.getColor(from: sender)
        playerOneView.backgroundColor = playerOneColor
        
        manaButton.newSelection(sender)
        
        pickColorView.isHidden = true
    }
    
    @IBAction func playerOneLessButton(_ sender: UIButton) {
        playerOneLife.text = lifeManager.lessLife(to: playerOneLife, less: 1)
    }
    
    
    @IBAction func playerOnePlusButton(_ sender: UIButton) {
        playerOneLife.text = lifeManager.moreLife(to: playerOneLife, plus: 1)
    }
    
    @IBAction func playerTwoLessButton(_ sender: UIButton) {
        playerTwoLife.text = lifeManager.lessLife(to: playerTwoLife, less: 1)
    }
    
    @IBAction func playerTwoPlusButton(_ sender: UIButton) {
        playerTwoLife.text = lifeManager.moreLife(to: playerTwoLife, plus: 1)
    }
    
    // MARK: - Gestures
    @IBAction func handlePan(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
            
        case UIGestureRecognizer.State.began, UIGestureRecognizer.State.changed:
            let translation = recognizer.translation(in: self.view)
            if let view = recognizer.view {
                view.center = CGPoint(x:view.center.x,
                                      y:view.center.y + translation.y)
            }
            recognizer.setTranslation(CGPoint.zero, in: self.view)
            break
        case UIGestureRecognizer.State.ended:
            let velocity = recognizer.velocity(in: self.view)
            let magnitude = sqrt(velocity.y * velocity.y)
            let slideMultiplier = magnitude / 200
            
            print("magnitude: \(magnitude), slideMultiplier: \(slideMultiplier)")
            
            
            
            if (magnitude > 1000) {
                let finalPoint = CGPoint(x: recognizer.view!.center.x,
                                         y: recognizer.view!.center.y + 1000)
                
                UIView.animate(withDuration: 1,
                               delay: 0,
                               options: UIView.AnimationOptions.curveEaseOut,
                               animations: {recognizer.view!.center = finalPoint },
                               completion: nil)
            }
            break
        default:
            break
        }
    }
    
    // MARK: -  Functions
    func updateView() {
        playerOneLife.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        pickColorView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        
        playerOneView.backgroundColor = playerTwoColor
        playerOneView.layer.cornerRadius = 60
        playerOneView.clipsToBounds = true
        
        playerTwoView.backgroundColor = playerTwoColor
        playerTwoView.layer.cornerRadius = 60
        playerTwoView.clipsToBounds = true
        
        pickColorView.layer.cornerRadius = 60
        pickColorView.clipsToBounds = true
        
        playerOneLife.text = life
        playerTwoLife.text = life
    }
}
