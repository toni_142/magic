//
//  StartGameViewController.swift
//  Magic support
//
//  Created by Toni García Alhambra on 07/01/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

class StartGameViewController: UIViewController {
    
    @IBOutlet weak var playButton: RoundedButton!
    @IBOutlet weak var localButton: RoundedButton!
    @IBOutlet weak var onlineButton: RoundedButton!
    @IBOutlet weak var viewerButton: RoundedButton!
    
    private var manaButton = ManaButton()
    
    private var colorSelected: UIColor?
    private var playPressed: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideButtons()
    }
    
    // MARK: - Buttons
    @IBAction func manaButtonPressed(_ sender: UIButton) {
        let color = manaButton.getColor(from: sender)
        colorSelected = color
        
        manaButton.newSelection(sender)
        
        fillButtonsWith(color: color)
        
        showPrimaryButtons()
        
        if playPressed {playButton.transform = CGAffineTransform(scaleX: 0, y: 0)} // Sin esta línea el botón PLAY aparece cada vez que se cambia de color
    }
    
    @IBAction func playButtonPressed(_ sender: UIButton) {
        UIView.animate(withDuration: 1.0,
                       animations: {
                        sender.frame = CGRect(x: sender.frame.origin.x, y: sender.frame.origin.y + 1000, width: sender.frame.size.width, height: sender.frame.size.height)
        })
        
        showSecondPlayButtons()
        playPressed = true
    }
    
    @IBAction func localButtonPressed(_ sender: UIButton) {
        playButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        performSegue(withIdentifier: "goToGameConfiguration", sender: self)
    }
    
    // MARK: - Functions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! GameConfigurationViewController
        destinationVC.selectedColor = colorSelected
    }
    
    private func fillButtonsWith(color: UIColor?) {
        playButton.backgroundColor = color
        localButton.backgroundColor = color
        onlineButton.backgroundColor = color
        viewerButton.backgroundColor = color
    }
    
    private func showPrimaryButtons() {
        animatedShowButton(for: playButton, withDelay: 0)
    }
    
    private func showSecondPlayButtons() {
        animatedShowButton(for: localButton, withDelay: 0.33)
        animatedShowButton(for: onlineButton, withDelay: 0.66)
        animatedShowButton(for: viewerButton, withDelay: 1.0)
    }
    
    private func hideButtons() {
        localButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        onlineButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        viewerButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        playButton.transform = CGAffineTransform(scaleX: 0, y: 0)
    }
    
    private func animatedShowButton (for button: UIButton, withDelay time: Double) {
        UIView.animate(withDuration: 0.6,
                       delay: time,
                       animations: {
                        button.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
}
