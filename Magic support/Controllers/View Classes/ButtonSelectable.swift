//
//  ButtonSelectable.swift
//  Magic support
//
//  Created by Toni García Alhambra on 08/01/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

@IBDesignable
class ButtonSelectable: UIButton {
    @IBInspectable var hasStroke: Bool = false {
        didSet {
            updateButton()
        }
    }
    
    func updateButton(){
        if hasStroke {
            let strokeTextAttributes = [
                NSAttributedString.Key.strokeColor: UIColor.white,
                NSAttributedString.Key.strokeWidth: -2.0] as [NSAttributedString.Key : Any]
            
            self.titleLabel?.attributedText = NSMutableAttributedString(string: self.titleLabel!.text!, attributes: strokeTextAttributes)
            self.backgroundColor = UIColor.black
            self.titleLabel?.textColor = UIColor.black
        } else {
            let strokeTextAttributes = [ NSAttributedString.Key.strokeWidth: -2.0] as [NSAttributedString.Key : Any]
            
            self.titleLabel?.attributedText = NSMutableAttributedString(string: self.titleLabel!.text!, attributes: strokeTextAttributes)
            self.layer.cornerRadius = 10
            self.clipsToBounds = true
            self.titleLabel?.textColor = UIColor.white
        }
    }
}
