//
//  RoundedButton.swift
//  Magic support
//
//  Created by Toni García Alhambra on 07/01/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedButton: UIButton {
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateButton()
        }
    }
    
    @IBInspectable var roundSize: CGFloat = 0 {
        didSet {
            updateButton()
        }
    }
    
    func updateButton(){
        if rounded {
            self.layer.cornerRadius = roundSize
            self.clipsToBounds = true
        }
    }
}

