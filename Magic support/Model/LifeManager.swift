//
//  LifeManager.swift
//  Magic support
//
//  Created by Toni García Alhambra on 09/01/2019.
//  Copyright © 2019 Toni García Alhambra. All rights reserved.
//

import UIKit

struct LifeManager {
    func moreLife(to label: UILabel, plus: Int) -> String? {
        if var number = Int(label.text!) {
            number += plus
            return String(number)
        }
        return nil
    }
    
    func lessLife(to label: UILabel, less: Int) -> String? {
        if var number = Int(label.text!) {
            number -= less
            return String(number)
        }
        return nil
    }
}
